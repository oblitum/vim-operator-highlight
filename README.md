This is a simple plugin that highlights operator characters of selected
languages.
I want characters like `+,-,/,*,.,:,=` etc. highlighted in C, C++, Rust and Go
by default.

The default color for operator highlighting is cyan, but this can be changed by
setting a different value to the `g:ophigh_color` variable for terminal and
`g:ophigh_color_gui` variable for gui. For instance, adding
`let g:ophigh_color = 226` and `let g:ophigh_color_gui = "#F6FF00"` to your
`vimrc` will highlight all operators with a bright yellow color.

You can also configure the plugin to highlight operators in more filetypes.
This is done by adding more filetypes to the
`g:ophigh_filetypes` list. For example: `add(g:ophigh_filetypes, 'markdown')`.

If you add that to your vimrc, then markdown files, besides the default ones,
will also be highlighted. NOTE: the element needs to be the filetype, not the
extension! You can get the filetype of the current file in Vim with
`:set filetype?`.
